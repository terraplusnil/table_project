import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class table {

    private JFrame frame;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTable table;
    private JScrollPane scrollPane;
    private JButton btnAdd;
    private JButton btnDelete;
    private JButton btnUpdate;
    private JButton btnClear;
    DefaultTableModel model;

    public static void main(String[]args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    table window = new table();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public table() {
        intialize();
    }

    private void intialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 539, 437);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(176, 196, 222));
        panel.setBounds(0,0,531,410);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        JLabel lblName = new JLabel("ID:");
        lblName.setBounds(21,83,46,14);
        panel.add(lblName);

        JLabel lblName_1 = new JLabel("Name:");
        lblName_1.setBounds(21,105,46,14);
        panel.add(lblName_1);

        JLabel lblContact = new JLabel("Contact:");
        lblContact.setBounds(21,127,46,14);
        panel.add(lblContact);

        JLabel lblCourse = new JLabel("Course:");
        lblCourse.setBounds(21,148,46,14);
        panel.add(lblCourse);

        textField = new JTextField();
        textField.setBounds(67,81,132,17);
        panel.add(textField);
        textField.setColumns(10);

        textField_1 = new JTextField();
        textField_1.setBounds(67,102,132,17);
        panel.add(textField_1);
        textField_1.setColumns(10);

        textField_2 = new JTextField();
        textField_2.setBounds(67,124,132,17);
        panel.add(textField_2);
        textField_2.setColumns(10);

        textField_3 = new JTextField();
        textField_3.setBounds(67,145,132,17);
        panel.add(textField_3);
        textField_3.setColumns(10);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(224,48,297,222);
        panel.add(scrollPane);

        table = new JTable();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int i = table.getSelectedRow();
                textField.setText(model.getValueAt(i,0).toString());
                textField_1.setText(model.getValueAt(i,1).toString());
                textField_2.setText(model.getValueAt(i,2).toString());
                textField_3.setText(model.getValueAt(i,3).toString());

            }
        });
        table.setBackground(new Color(176,196,222));
        model = new DefaultTableModel();
        Object[] column = {"ID","Name","Contact","Course"};
        final Object[] row = new Object[4];
        model.setColumnIdentifiers(column);
        table.setModel(model);
        scrollPane.setViewportView(table);

        btnAdd = new JButton("Add");
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(textField.getText().equals("")||textField_1.getText().equals("")||textField_2.getText().equals("")||textField_3.getText().equals("")){
                    JOptionPane.showMessageDialog(null,"Please Fill In The Information");
                } else
                {
                    row[0] = textField.getText();
                    row[1] = textField_1.getText();
                    row[2] = textField_2.getText();
                    row[3] = textField_3.getText();
                    model.addRow(row);

                    textField.setText("");
                    textField_1.setText("");
                    textField_2.setText("");
                    textField_3.setText("");
                    JOptionPane.showMessageDialog(null, "Added Successfully");
                }


            }
        });
        btnAdd.setBounds(10,280,93,23);
        panel.add(btnAdd);

        btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table.getSelectedRow();
                if(i>=0){
                    model.removeRow(i);
                    JOptionPane.showMessageDialog(null,"Deleted Succsessfully");
                }
                else{
                    JOptionPane.showMessageDialog(null,"Please Select a Row First");
                }
            }
        });
        btnDelete.setBounds(10,324,93,23);
        panel.add(btnDelete);

        btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int i = table.getSelectedRow();
                model.setValueAt(textField.getText(), i, 0);
                model.setValueAt(textField_1.getText(), i, 1);
                model.setValueAt(textField_2.getText(), i, 2);
                model.setValueAt(textField_3.getText(), i, 3);

            }
        });
        btnUpdate.setBounds(133,280,70,23);
        panel.add(btnUpdate);

        btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField.setText("");
                textField_1.setText("");
                textField_2.setText("");
                textField_3.setText("");
            }
        });
        btnClear.setBounds(133,324,70,23);
        panel.add(btnClear);
    }

    }
